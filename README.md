# Kittle

An interactive world of pets, built for children suffering from chronic illness, pain and/or anxiety.

## A Game

Kittle comes as an app that can be downloaded and played on any device with AR and face-tracking support. It offers an environment where children can interact with artificially intelligent pets that create distractions based on the child's emotional responses. These distractions can help the child cope with their illness or fear.

## A Research Innovation

Through the face tracking and analytics, we can learn more about children's emotional responses to stimuli. It can lead us towards a deeper understanding of child behavior and guide us to building more meaningful relationships and interactions with them.
